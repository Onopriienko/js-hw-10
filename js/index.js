let firstPassword = document.getElementById('first-enter-password');
let secondPassword = document.getElementById('second-enter-password');

document.getElementById('mainBtn').addEventListener('click', function () {
    if (firstPassword.value !== secondPassword.value){
        document.getElementById("errorText").style.visibility = 'visible';
    }else{
        alert("You are welcome");
        document.getElementById("errorText").style.visibility = 'hidden';
    }
});
function showHidePass() {
    if ((this.className === 'fas fa-eye icon-password')){
        this.className = 'fas fa-eye-slash icon-password';
    }else{
        this.className = 'fas fa-eye icon-password';
    }
}

function show1(){
    if (firstPassword.type === 'password'){
        firstPassword.type = 'text';
    } else{
        firstPassword.type = 'password';
    }
}
function show2(){
    if (secondPassword.type === 'password'){
        secondPassword.type = 'text';
    } else{
        secondPassword.type = 'password';
    }
}

document.getElementById("visible").addEventListener("click",showHidePass);
document.getElementById("hidden").addEventListener("click",showHidePass);
document.getElementById("visible").addEventListener("click",show1);
document.getElementById("hidden").addEventListener("click",show2);
